from flask import Flask, request
from werkzeug.contrib.cache import SimpleCache
import requests 
import re
import json
import os
from time import time, sleep
from Crypto.PublicKey import RSA
from Crypto.Signature import pkcs1_15
from Crypto.Hash      import SHA384
from uuid import uuid4

app = Flask(__name__)
# This cache is used to store tokens for their lifetime to avoid creating a new one for each request
cache = SimpleCache()


# By default, the proxy looks for credentials in environment variables APPNEXUS_USERNAME and APPNEXUS_PASSWORD
# If unfound, it reads credentials.json, which is generated by the install script
# If no credentials are found in either scenarii, exit
credentials = {}
if os.environ.get('APPNEXUS_USERNAME') is not None and os.environ.get('APPNEXUS_PASSWORD') is not None:
    credentials['username']=os.environ.get('APPNEXUS_USERNAME')
    credentials['password']=os.environ.get('APPNEXUS_PASSWORD')
else: 
    try: 
        credentials = json.load(open('credentials.json', 'r'))
        assert credentials.get('username') is not None and credentials.get('password') is not None
    except: 
        print("Error - Could not load credentials")
        exit()




# Specify the AppNexus hostname and which method / endpoint combinations are allowed
SITE_NAME         = 'https://api.appnexus.com'
ALLOWED_ENDPOINTS = {
    'GET'   : r'^/apd-api|^/usage', 
    'POST'  : r'^/apd-api|^/segment',
    'PATCH' : r'a^', 
    'PUT'   : r'a^', 
    'DELETE': r'a^' 
    }
HTTP_METHODS = [_m for _m in ALLOWED_ENDPOINTS]

def save_file_and_check_signature(request):

    # Gather all the elements that were included in the signature: path, uuid, data, file content
    if request.full_path.endswith('?'):
        path_bytes          = request.full_path[:-1].encode('utf-8')
    else: 
        path_bytes          = request.full_path.encode('utf-8')

    orion_signature_hex = request.headers.get('orion-signature', '')
    orion_uuid          = request.headers.get('orion-uuid', '')
    
    if request.data is None: 
        data_bytes = b''
    else: 
        data_bytes = request.data

    

    if request.method == 'POST' and 'file' in request.files: 
        filename = f'./tmp/{str(uuid4())}'
        try: 
            os.makedirs('./tmp')
        except: 
            pass
        request.files['file'].save(filename)
        with open(filename, mode='rb') as file: # b is important -> binary
            filecontent_bytes = file.read()
    else: 
        filename = None
        filecontent_bytes = b''

    # Combine all elements together
    data_combined  = orion_uuid.encode('utf-8') \
                   + path_bytes                 \
                   + data_bytes                 \
                   + filecontent_bytes

    # Check that the signature matches the content submitted
    public_key = RSA.importKey(open('public_key.der', 'rb').read())
    try:
        pkcs1_15.new(public_key).verify(
            SHA384.new(data_combined), 
            bytes.fromhex(orion_signature_hex)
            )
        return True, filename
    except: 
        return False, filename
 
def get_token():
    
    # Check if a token exists in the cache
    token = cache.get('token')
    if token is not None: 
        # Using saved token
        return token
    else:
        # Getting new token from AppNexus
        auth_data = json.dumps(
            {
                'auth': {
                    'username': credentials.get('username'), 
                    'password': credentials.get('password')
                    }
                }
            )

        auth_response = requests.post(
            f"{SITE_NAME}/auth", 
            data = auth_data
            )


        try: 
            assert auth_response.status_code == 200
            token = auth_response.json().get('response').get('token')
            # Store the token in cache for 90mn (max lifetime is 120mn)
            cache.set('token', token, timeout = 90*60) 
            return token
        except Exception as e:
            print(e) 
            return None
    
@app.route('/', defaults={'path': ''}, methods = HTTP_METHODS)
@app.route('/<path:path>'            , methods = HTTP_METHODS)
def proxy(path):

    # This human-readable healtcheck allows to check that the proxy is up 
    # and that the credentials are valid.
    if path == 'healthcheck': 
        if get_token() is not None: 
            return f"OK -- The proxy is operational; connected as {credentials.get('username')}."
        else: 
            return f"ERROR - Credentials are invalid for user: {credentials.get('username')}. "

    # Check that the method / endpoint combination called is allowed. 
    if not re.match(
        ALLOWED_ENDPOINTS.get(request.method), 
        request.full_path
        ):
        return "Endpoint not allowed", 403

    # Check that the content matches the signature
    # Since the file is checked too, this method saves the file 
    # on disk and retursn a filename
    
    check, filename = save_file_and_check_signature(request)
    if not check:
        return "Signature is invalid", 403

    # Prepare the file dictionnary, if a file was uploaded
    if filename is not None: 
        files = {'file': open(filename, 'rb')}
    else: 
        files = {}

    # Prepare the authentication header
    token = get_token()
    if token is None:
        return "Could not get a token from AppNexus. Check the credentials.", 403
    else: 
        headers = {'Authorization': token}
    
    if path == 'usage': 
        try: 
            stage = "Report Request"
            report_request = requests.post(
                f'{SITE_NAME}/report',
                headers = headers,
                json    = {"report": 
                                {
                                    "format": "csv",
                                    "report_interval": "last_30_days",
                                    "columns": ["day", "segment_id", "segment_name", "segment_code", "imps", "clicks", "total_convs", "post_view_convs", "post_click_convs", "media_cost"],
                                    "report_type": "buyer_segment_performance"
                                }
                            }
                )

            if report_request.json().get('response', {}).get('status')!='OK': 
                return f"Report request status is not OK. \n {report_request.text}", 500

            report_id = report_request.json().get('response', {}).get('report_id')

            if report_id is None: 
                return f"Report ID not provided by AppNexus. \n {report_request.text}"

            stage = "Report Status"
            trials = 0
            report_ready = False
            while trials < 12 and not report_ready: 
                report_status_request = requests.get(
                    f'{SITE_NAME}/report?id={report_id}',
                    headers = headers
                    )
                if report_status_request.json().get('response', {}).get('execution_status')=="ready":
                    report_ready = True
                else: 
                    trials += 1
                    sleep(10)

            if not report_ready:
                return "Report Status was not ready after 120 seconds.", 500

            stage = "Report Download"
            report_content_request = requests.get(
                    f'{SITE_NAME}/report-download?id={report_id}',
                    headers = headers
                    )

            if report_content_request.status_code != 200: 
                return f"report-downlad returned an error code: {report_content_request.status_code}", 500

            else: 
                lines = report_content_request.text.split('\r\n')
                filtered_lines = [lines[0]]+\
                                 [_line 
                                  for _line 
                                  in lines 
                                  if len(_line.split(','))>=3 
                                  and _line.split(',')[3].startswith('orion_')
                                  ]

                return '\r\n'.join(filtered_lines), 200


        except Exception as e: 
            return f"Could not retrieve segment usage report, unknown error. Stage: {stage}; Error: {str(e)}", 500



    
    else:
        # Submit the request
        r = requests.request(
            method  = request.method, 
            url     = f'{SITE_NAME}{request.full_path}', 
            data    = request.data, 
            files   = files,
            headers = headers
            )

        # Delete the uploaded file, if any.
        if filename is not None: 
            os.remove(filename)

        # Forward the response to the client. 
        return r.content


print(f"Starting service, username is: {credentials.get('username')}")