FROM ubuntu

RUN apt-get update --fix-missing &&   \
    apt-get -y install python3-pip && \
    apt-get -y install locales &&     \
    apt-get -y install libssl-dev &&  \
    locale-gen en_US.UTF-8
    
RUN pip3 install pycryptodome==3.7.3 \
                 flask               \
                 werkzeug            \
                 requests            \
                 uwsgi

ADD . /usr/local/bin/orion-exporters-appnexus-proxy/

WORKDIR /usr/local/bin/orion-exporters-appnexus-proxy/

ENV LANG en_US.UTF-8
ENV LANGUAGE en_US:en
ENV LC_ALL en_US.UTF-8

EXPOSE 443

CMD openssl genrsa -out https.key 2048 &&                                                                     \
    openssl req -new -key https.key -out https.csr -subj '/C=FR/ST=IDF/L=Paris/CN=www.orion-semantics.com' && \
    openssl x509 -req -days 365 -in https.csr -signkey https.key -out https.crt &&                            \  
    #python3 wsgi.py
    uwsgi --master --https 0.0.0.0:443,https.crt,https.key -w wsgi:app