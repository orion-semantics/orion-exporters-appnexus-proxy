#!/bin/bash



echo ""
echo ""
echo "######################################################################"
echo "#                                                                    #"
echo "# Installing required libraries: libssl-dev and python3              #"
echo "#                                                                    #"
echo "######################################################################"
echo ""
echo ""

sudo apt-get update
sudo apt-get install -y libssl-dev python3-pip git

echo ""
echo ""
echo "######################################################################"
echo "#                                                                    #"
echo "# Installing to /usr/local/bin/orion-appnexus-proxy/                 #"
echo "#                                                                    #"
echo "######################################################################"
echo ""
echo ""

cd /usr/local/bin/
sudo git clone https://gitlab.com/orion-semantics/orion-exporters-appnexus-proxy.git 
cd /usr/local/bin/orion-exporters-appnexus-proxy



echo ""
echo ""
echo "######################################################################"
echo "#                                                                    #"
echo "# Generating an SSL key to grant a secure connexion to the proxy     #"
echo "#                                                                    #"
echo "######################################################################"
echo ""
echo ""

openssl genrsa -out https.key 2048
sudo openssl req -new -key https.key -out https.csr -subj '/C=FR/ST=IDF/L=Paris/CN=www.orion-semantics.com'
sudo openssl x509 -req -days 365 -in https.csr -signkey https.key -out https.crt

echo ""
echo ""
echo "######################################################################"
echo "#                                                                    #"
echo "# Installing python and creating virtual environment                 #"
echo "#                                                                    #"
echo "######################################################################"
echo ""
echo ""

sudo pip3 install virtualenv
sudo virtualenv venv
source venv/bin/activate
pip3 install -r requirements.txt

echo ""
echo ""
echo "######################################################################"
echo "#                                                                    #"
echo "# Saving AppNexus credentials                                        #"
echo "#                                                                    #"
echo "######################################################################"
echo ""
echo ""


echo AppNexus username: 
read username
echo AppNexus password: 
read password
sudo echo "{\"username\":\""$username"\",\"password\":\""$password"\"}" > /usr/local/bin/orion-exporters-appnexus-proxy/credentials.json

echo ""
echo ""
echo "######################################################################"
echo "#                                                                    #"
echo "# Creating a service and enabling it at startup                      #"
echo "#                                                                    #"
echo "######################################################################"
echo ""
echo ""

sudo mv orion-exporters-appnexus-proxy.service /etc/systemd/system/
sudo systemctl start orion-exporters-appnexus-proxy
sudo systemctl enable orion-exporters-appnexus-proxy
sudo systemctl status orion-exporters-appnexus-proxy --no-pager


echo ""
echo ""
echo "######################################################################"
echo "#                                                                    #"
echo "# Finished !                                                         #"
echo "#                                                                    #"
echo "# Check https://proxy-ip/healthcheck to ensure proxy is operational  #"
echo "#                                                                    #"
echo "######################################################################"
echo ""
echo ""
