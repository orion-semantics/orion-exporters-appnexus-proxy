# Installation instructions 

## Step 1 : Launch a virtual machine

* Start an instance with a Ubuntu 18 distribution
* Open port 443

## Step 2 : Install our Appnexus Proxy via Docker 

* SSH into the instance.

* Install Docker by running:
```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sh get-docker.sh
```

* Once Docker is installed, run the proxy with the following command (replace \<USERNAME> and \<PASSWORDS> with valid AppNexus credentials):
```bash
sudo docker run -e APPNEXUS_USERNAME=<USERNAME> -e APPNEXUS_PASSWORD=<PASSWORD> -p 443:443 orionsemantics/appnexus-proxy:latest
```

## Step 3 : Verify the proxy is functionnal

Run this instruction from your local shell to ensure the proxy is functional: 

```bash
curl -k https://<INSTANCE_IP>:443/healthcheck
```

The proxy should return the following response: 

OK -- The proxy is operational; connected as \<USERNAME>.
